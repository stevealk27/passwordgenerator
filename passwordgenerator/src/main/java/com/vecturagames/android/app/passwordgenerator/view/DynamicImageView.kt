package com.vecturagames.android.app.passwordgenerator.view

import android.content.Context
import android.util.AttributeSet
import android.view.View

import androidx.appcompat.widget.AppCompatImageView

class DynamicImageView(context: Context, attrs: AttributeSet) : AppCompatImageView(context, attrs) {

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val d = this.drawable

        if (d != null) {
            val width = View.MeasureSpec.getSize(widthMeasureSpec)
            val height = Math.ceil((width * d.intrinsicHeight.toFloat() / d.intrinsicWidth).toDouble()).toInt()
            this.setMeasuredDimension(width, height)
        }
        else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        }
    }

}
